#!/bin/bash



SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
cd "$(dirname "$0")"

check=$(crontab -l | grep "motion_run.sh")
if [ "$check" == "" ]; then
    cat <(crontab -l) <(echo "* * * * * $SCRIPTPATH/motion_run.sh") | crontab -
fi

LOCAL=$(git rev-parse @)
git pull
LOCAL2=$(git rev-parse @)

echo `date +"[%Y%m%d-%T]"`"[Start]LOCAL:"$LOCAL", LOCAL2:"$LOCAL2 >> $SCRIPTPATH"/run.log"

check=$(ps aux | grep motion.py)

if [ $LOCAL = $LOCAL2 ]; then
    echo "Up-to-date"
    if [[ $check = *"python3"* ]]; then
        echo "[Running] motion.py"
    else
        echo `date +"[%Y%m%d-%T]"`"[Start] motion.py" >> $SCRIPTPATH"/run.log"
        $SCRIPTPATH"/motion.py" > /dev/null 2>&1 &
    fi
else
    echo `date +"[%Y%m%d-%T]"`"[Kill] motion.py" >> $SCRIPTPATH"/run.log"
    sentence=$(pgrep -f "motion.py")
    for pid in $sentence
    do
        kill -9 $pid
    done
    echo `date +"[%Y%m%d-%T]"`"[Start]motion.py" >> $SCRIPTPATH"/run.log"
    $SCRIPTPATH"/motion.py" > /dev/null 2>&1 &
fi
