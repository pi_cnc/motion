#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import RPi.GPIO as GPIO
import time

gpioNumS2=14
gpioNumS3=15
gpioNumColor=18
NUM_CYCLES = 10

colorDetector = "GREEN"

def initColoreSensor():
    GPIO.cleanup()
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(gpioNumColor, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(gpioNumS3, GPIO.OUT)
    GPIO.setup(gpioNumS2, GPIO.OUT)
    # LL red
    # HL blue
    # HH green
    if colorDetector == "RED":
        GPIO.output(gpioNumS2,GPIO.LOW)
        GPIO.output(gpioNumS3,GPIO.LOW)
    if colorDetector == "BLUE":
        GPIO.output(gpioNumS2,GPIO.HIGH)
        GPIO.output(gpioNumS3,GPIO.LOW)
    if colorDetector == "GREEN":
        GPIO.output(gpioNumS2,GPIO.HIGH)
        GPIO.output(gpioNumS3,GPIO.HIGH)

def job_motion():
    one = getColorNum()
    time.sleep(5)
    two = getColorNum()
    if one > two:
        print(one)
    else:
        print(two)

def wait_for_it(num):
    level = GPIO.input(num)
    while (GPIO.input(num) == level):
        time.sleep(0.01)

def getColorNum():
    time.sleep(0.3)
    colorStart = time.time()
    for impulse_count in range(NUM_CYCLES):
        try:
            GPIO.wait_for_edge(gpioNumColor, GPIO.FALLING)
        except RuntimeError as e:
            if "waiting for edge" in str(e):
                wait_for_it(gpioNumColor)
            else:
                raise e  # Re-raise the exception if it's not the expected one
    duration = time.time() - colorStart
    colorNum = (NUM_CYCLES / duration)
    return colorNum

if __name__ == "__main__":
    initColoreSensor()
    for num in range(1, 30):
        job_motion()
        time.sleep(5)
