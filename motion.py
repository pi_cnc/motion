#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import os.path
import requests
import time
import json
import socket
import sys
try:
    import RPi.GPIO as GPIO
    isPi = True
except Exception:
    isPi = False

from datetime import datetime
import logging
from apscheduler.schedulers.background import BackgroundScheduler

hostname = socket.gethostname()
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(("8.8.8.8", 80))
ip = s.getsockname()[0]
s.close()

pwdPath = os.path.dirname(os.path.realpath(__file__)) + "/"

sched = BackgroundScheduler({
    'apscheduler.job_defaults.coalesce': 'false',
    'apscheduler.job_defaults.max_instances': '1',
    'apscheduler.timezone': 'Asia/Taipei',
})

Webhook_url = "https://cncbot.goportal.one/log"
logJson = {
    "ip":ip,
    "hostname":hostname,
}


Webhook_color_url = "https://cncbot.goportal.one/color?hostname=" + hostname

global isCall
isCall = False
global timeout
global colorDetector
colorDetector = "GREEN"

try:
    colorDetector = requests.get(Webhook_color_url).text
except Exception:
    colorDetector = "GREEN"

print(colorDetector)

gpioNumS2=14
gpioNumS3=15
gpioNumColor=18
NUM_CYCLES = 10

def initColoreSensor():
    GPIO.cleanup()
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(gpioNumColor, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(gpioNumS3, GPIO.OUT)
    GPIO.setup(gpioNumS2, GPIO.OUT)
    # LL red
    # HL blue
    # HH green
    if colorDetector == "RED":
        GPIO.output(gpioNumS2,GPIO.LOW)
        GPIO.output(gpioNumS3,GPIO.LOW)
    elif colorDetector == "BLUE":
        GPIO.output(gpioNumS2,GPIO.HIGH)
        GPIO.output(gpioNumS3,GPIO.LOW)
    else:
        GPIO.output(gpioNumS2,GPIO.HIGH)
        GPIO.output(gpioNumS3,GPIO.HIGH)

if isPi:
    initColoreSensor()

def wait_for_it(num):
    level = GPIO.input(num)
    while (GPIO.input(num) == level):
        time.sleep(0.01)

def getColorNum():
    if not isPi:
        return 3000
    time.sleep(0.3)
    colorStart = time.time()
    for impulse_count in range(NUM_CYCLES):
        try:
            GPIO.wait_for_edge(gpioNumColor, GPIO.FALLING)
        except RuntimeError as e:
            if "waiting for edge" in str(e):
                wait_for_it(gpioNumColor)
            else:
                raise e  # Re-raise the exception if it's not the expected one
    duration = time.time() - colorStart
    colorNum = (NUM_CYCLES / duration)
    return colorNum


def merge_two_dicts(x, y):
    if x and y:
        z = x.copy()
        z.update(y)
        return z
    elif x:
        return x
    elif y:
        return y
    else:
        return None

def log(colorNum, isStart):
    now = datetime.now()

    json = merge_two_dicts(logJson,{
        "colorDetector":colorDetector,
        "colorNum":colorNum,
        "time":now.strftime("%Y/%m/%d %H:%M:%S"),
    })

    if isStart:
        json["hostname"] = "[start]" + json["hostname"]

    response = requests.post(Webhook_url, json=json)

    logger.info("[log][%s] %s" %(response.status_code, str(json)))

def job_motion():
    one = getColorNum()
    time.sleep(5)
    two = getColorNum()
    if one > two:
        log(one, False)
    else:
        log(two, False)

if __name__ == "__main__":
    print("Run:", hostname, ip, isPi, colorDetector)

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)

    fh = logging.FileHandler(pwdPath + 'motion.log')
    fh.setLevel(logging.DEBUG)
    logger.addHandler(fh)

    formatter = logging.Formatter("[%(asctime)s][%(levelname)s] %(message)s")

    ch.setFormatter(formatter)

    # add ch to logger
    logger.addHandler(ch)

    logger.info("[Run][%s] ip:%s, isPi:%s, colorDetector:%s" %(hostname, ip, isPi, colorDetector))
    log(getColorNum(), True)

    if isPi:
        sched.add_job(job_motion,
                    'cron',
                    minute='*',
                    name='job_motion')

        sched.start()

    try:
        while True:
            time.sleep(2)
            if not isPi:
                job_motion()
    except (KeyboardInterrupt, SystemExit):
        sched.shutdown()
    finally:
        if isPi:
            GPIO.cleanup()
